﻿using System.ComponentModel.DataAnnotations;

namespace Ligi.Web.Security.Models
{
    public class ChangeUsernameModel
    {
        [Required]
        public string NewUsername { get; set; }
    }
}