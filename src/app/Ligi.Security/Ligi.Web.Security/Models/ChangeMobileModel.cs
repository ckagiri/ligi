﻿namespace Ligi.Web.Security.Models
{
    public class ChangeMobileModel
    {
        public string Current { get; set; }
        public string NewMobilePhone { get; set; }
    }
}