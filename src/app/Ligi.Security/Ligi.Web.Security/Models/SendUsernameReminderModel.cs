﻿using System.ComponentModel.DataAnnotations;

namespace Ligi.Web.Security.Models
{
    public class SendUsernameReminderModel
    {
        [Required]
        public string Email { get; set; }
    }
}