﻿namespace Ligi.Web.Security.Models
{
    public class ChangeEmailModel
    {
        public string NewEmail { get; set; }
    }
}