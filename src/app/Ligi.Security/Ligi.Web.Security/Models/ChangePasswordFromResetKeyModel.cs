﻿using System.ComponentModel.DataAnnotations;

namespace Ligi.Web.Security.Models
{
    public class ChangePasswordFromResetKeyModel
    {
        [Required]
        public string Password { get; set; }
        
        [Required]
        public string ConfirmPassword { get; set; }
        
        public string Key { get; set; }
    }
}