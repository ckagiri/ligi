﻿using System.ComponentModel.DataAnnotations;

namespace Ligi.Web.Security.Models
{
    public class ChangeEmailFromKeyModel
    {
        [Required]
        public string Password { get; set; }

        public string Key { get; set; }
    }
}