﻿using System.ComponentModel.DataAnnotations;

namespace Ligi.Web.Security.Models
{
    public class ChangeMobileFromCodeModel
    {
        [Required]
        public string Code { get; set; }
    }
    
}