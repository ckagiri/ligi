﻿using System.Web.Mvc;
using System.Web.Routing;

namespace Ligi.Web.Security
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "SPA",
                url: "{*catchall}",
                defaults: new { controller = "Home", action = "Index" },
                namespaces: new[] { "Ligi.Web.Security.Controllers" }
            );

            routes.MapRoute(
                name: "Short",
                url: "{action}/{id}",
                defaults: new { controller = "Home", action = "Index" },
                namespaces: new[] { "Ligi.Web.Security.Controllers" }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional },
                namespaces:new[]{"Ligi.Web.Security.Controllers"}
            );
        }
    }
}
