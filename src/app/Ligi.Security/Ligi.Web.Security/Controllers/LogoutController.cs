﻿using System.Web.Http;
using BrockAllen.MembershipReboot;

namespace Ligi.Web.Security.Controllers
{
    public class LogoutController : ApiController
    {
        private readonly AuthenticationService _authService;

        public LogoutController(AuthenticationService authSvc)
        {
            _authService = authSvc;
        }

        [Route("Logout")]
        [HttpPost]
        public IHttpActionResult Logout()
        {
            _authService.SignOut();
            return Ok();
        }
    }
}