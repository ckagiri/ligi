﻿using System.Web.Http;

namespace Ligi.Web.Security.Controllers
{
    [Authorize]
    [RoutePrefix("api/Account")]
    public class AccountController : ApiController
    {
    }
}
