﻿using System;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using BrockAllen.MembershipReboot;
using Ligi.Web.Security.Models;

namespace Ligi.Web.Security.Controllers
{
    [AllowAnonymous]
    public class RegisterController : ApiController
    {
        private readonly UserAccountService _userAccountService;
        private readonly AuthenticationService _authService;

        public RegisterController(AuthenticationService authSvc)
        {
            _authService = authSvc;
            _userAccountService = authSvc.UserAccountService;
        }

        [Route("api/account/register")]
        [HttpPost]
        public IHttpActionResult Register(RegisterModel model)
        {
            var message = new StringBuilder();
            if (string.IsNullOrEmpty(model.Email))
                return BadRequest("Email cannot be empty.");
            if (string.IsNullOrEmpty(model.Username))
                return BadRequest("Username cannot be empty.");
            if (string.IsNullOrEmpty(model.Password))
                return BadRequest("Password cannot be empty.");
            if (model.Password != model.ConfirmPassword)
                return BadRequest("Password and confirm password do not match.");
            try
            {
                var account = _userAccountService.CreateAccount(model.Username, model.Password, model.Email);
            }
            catch (Exception ex)
            {
                message.Append(ex.Message);
                return Json(new { success = false, message });
            }
            message.AppendFormat("User {0} is created successfully.", model.Username);
            return Json(new { success = true, message});
        }

        [Route("api/account/confirmemail")]
        [HttpPost]
        public IHttpActionResult ConfirmEmail(ChangeEmailFromKeyModel model)
        {
            var message = new StringBuilder();
            try
            {
                UserAccount account;
                _userAccountService.VerifyEmailFromKey(model.Key, model.Password, out account);
            }
            catch (Exception ex)
            {
                message.Append(ex.Message);
                return Json(new { success = false, message });
            }
            message.Append("Email address was confirmed.");
            return Json(new { success = true, message });
        }

        [Route("api/account/verifyemail")]
        [HttpPost]
        public IHttpActionResult VerifyEmail()
        {
            var message = new StringBuilder();
            try
            {
                _userAccountService.RequestAccountVerification(User.GetUserID());
            }
            catch (Exception ex)
            {
                message.Append(ex.Message);
                return Json(new { success = false, message });
            }
            message.Append("Email verification sent.");
            return Json(new { success = true, message });
        }

        [Route("api/account/cancelverificationrequest")]
        [HttpPost]
        public IHttpActionResult CancelVerificationRequest(string id)
        {
            var message = new StringBuilder();
            try
            {
                bool closed;
                _userAccountService.CancelVerification(id, out closed);
            }
            catch (Exception ex)
            {
                message.Append(ex.Message);
                return Json(new { success = false, message });
            }
            message.Append("The request that was emailed to you is now cancelled.");
            return Json(new { success = true, message });
        }
    }
}