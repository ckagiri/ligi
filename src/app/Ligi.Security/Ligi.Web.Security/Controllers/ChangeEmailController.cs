﻿using System.Text;
using System.Web.Http;
using BrockAllen.MembershipReboot;
using Ligi.Web.Security.Models;

namespace Ligi.Web.Security.Controllers
{
    public class ChangeEmailController : ApiController
    {
        private readonly UserAccountService _userAccountService;

        public ChangeEmailController(AuthenticationService authSvc)
        {
            _userAccountService = authSvc.UserAccountService;
        }

        [Route("ChangeEmail")]
        [HttpPost]
        public IHttpActionResult ChangeEmail(ChangeEmailModel model)
        {
            var message = new StringBuilder();
            _userAccountService.ChangeEmailRequest(User.GetUserID(), model.NewEmail);
            if (_userAccountService.Configuration.RequireAccountVerification)
            {
                 message.Append("ChangeRequestSuccess");
            }
            else
            {
                message.Append("Success");
            }
            return Json<object>(new { sucess = true, message = message });
        }
    }
}