﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ligi.Core.Domain;
using Ligi.Data.Data;
using Ligi.Data.Models;
using Microsoft.AspNet.Identity;

namespace Ligi.Data.Security
{
    public class AuthRepository : IAuthRepository
    {
        private readonly IDbContext _ctx;

        private readonly UserManager<ApplicationUser> _userManager;

        public AuthRepository(IDbContext context, UserManager<ApplicationUser> userManager)
        {
            _ctx = context;
            _userManager = userManager;
        }

        public IdentityResult RegisterUser(UserModel userModel)
        {
            var user = new ApplicationUser
            {
                UserName = userModel.UserName
            };

            var result = _userManager.Create(user, userModel.Password);

            return result;
        }

        public ApplicationUser FindUser(string userName, string password)
        {
            var user =_userManager.Find(userName, password);

            return user;
        }

        public Client FindClient(string clientId)
        {
            var client = _ctx.DbSet<Client>().Find(clientId);

            return client;
        }

        public bool AddRefreshToken(RefreshToken token)
        {

           var existingToken = _ctx.DbSet<RefreshToken>().SingleOrDefault(r => 
               r.Subject == token.Subject && r.ClientId == token.ClientId);

           if (existingToken != null)
           {
             var result = RemoveRefreshToken(existingToken);
           }
          
            _ctx.DbSet<RefreshToken>().Add(token);

            return _ctx.SaveChanges() > 0;
        }

        public bool RemoveRefreshToken(string refreshTokenId)
        {
           var refreshToken = _ctx.DbSet<RefreshToken>().Find(refreshTokenId);

           if (refreshToken != null) {
               _ctx.DbSet<RefreshToken>().Remove(refreshToken);
               return _ctx.SaveChanges() > 0;
           }

           return false;
        }

        public bool RemoveRefreshToken(RefreshToken refreshToken)
        {
            _ctx.DbSet<RefreshToken>().Remove(refreshToken);
             return _ctx.SaveChanges() > 0;
        }

        public RefreshToken FindRefreshToken(string refreshTokenId)
        {
            var refreshToken = _ctx.DbSet<RefreshToken>().Find(refreshTokenId);

            return refreshToken;
        }

        public List<RefreshToken> GetAllRefreshTokens()
        {
             return  _ctx.DbSet<RefreshToken>().ToList();
        }
    }
}