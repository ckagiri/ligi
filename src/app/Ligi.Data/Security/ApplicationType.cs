﻿namespace Ligi.Data.Security
{
    public enum ApplicationType
    {
        JavaScript = 0,
        NativeConfidential = 1
    };
    
}