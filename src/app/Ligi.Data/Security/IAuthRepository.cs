﻿using System;
using System.Collections.Generic;
using Ligi.Core.Domain;
using Ligi.Data.Models;
using Microsoft.AspNet.Identity;

namespace Ligi.Data.Security
{
    public interface IAuthRepository
    {
        IdentityResult RegisterUser(UserModel userModel);
        ApplicationUser FindUser(string userName, string password);
        Client FindClient(string clientId);
        bool AddRefreshToken(RefreshToken token);
        bool RemoveRefreshToken(string refreshTokenId);
        bool RemoveRefreshToken(RefreshToken refreshToken);
        RefreshToken FindRefreshToken(string refreshTokenId);
        List<RefreshToken> GetAllRefreshTokens();
    }
}
