﻿using System;
using Ligi.Data.Configuration;
using Ligi.Data.Infrastructure.DependencyManagement;

namespace Ligi.Data.Infrastructure
{
    /// <summary>
    /// Classes implementing this interface can serve as a portal for the 
    /// various services composing the Ligi engine. Edit functionality, modules
    /// and implementations access most Ligi functionality through this 
    /// interface.
    /// </summary>
    public interface IEngine
    {
        ContainerManager ContainerManager { get; }
        
        /// <summary>
        /// Initialize components and plugins in the Ligi environment.
        /// </summary>
        /// <param name="config">Config</param>
        void Initialize(LigiConfig config);

        T Resolve<T>() where T : class;

        object Resolve(Type type);

        T[] ResolveAll<T>();
    }
}
