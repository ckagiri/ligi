﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using Autofac;
using Ligi.Data.Configuration;
using Ligi.Data.Infrastructure.DependencyManagement;

namespace Ligi.Data.Infrastructure
{
    public class LigiEngine : IEngine
    {
        private ContainerManager _containerManager;
        
        /// <summary>
		/// Creates an instance of the content engine using default settings and configuration.
		/// </summary>
		public LigiEngine() : this(new ContainerConfigurer())
		{ }

		public LigiEngine(ContainerConfigurer configurer)
		{
            var config = ConfigurationManager.GetSection("LigiConfig") as LigiConfig;
            InitializeContainer(configurer, config);
		}

        private void RunStartupTasks()
        {
            var typeFinder = _containerManager.Resolve<ITypeFinder>();
            var startUpTaskTypes = typeFinder.FindClassesOfType<IStartupTask>();
            var startUpTasks = new List<IStartupTask>();
            foreach (var startUpTaskType in startUpTaskTypes)
                startUpTasks.Add((IStartupTask)Activator.CreateInstance(startUpTaskType));
            //sort
            startUpTasks = startUpTasks.AsQueryable().OrderBy(st => st.Order).ToList();
            foreach (var startUpTask in startUpTasks)
                startUpTask.Execute();
        }
        
        private void InitializeContainer(ContainerConfigurer configurer, LigiConfig config)
        {
            var builder = new ContainerBuilder();

            _containerManager = new ContainerManager(builder.Build());
            configurer.Configure(this, _containerManager, config);
        }

        /// <summary>
        /// Initialize components and plugins in the ligi environment.
        /// </summary>
        /// <param name="config">Config</param>
        public void Initialize(LigiConfig config)
        {
            //startup tasks
            if (!config.IgnoreStartupTasks)
            {
                RunStartupTasks();
            }
        }

        public T Resolve<T>() where T : class
		{
            return ContainerManager.Resolve<T>();
		}

        public object Resolve(Type type)
        {
            return ContainerManager.Resolve(type);
        }
        
        public T[] ResolveAll<T>()
        {
            return ContainerManager.ResolveAll<T>();
        }

        public ContainerManager ContainerManager
        {
            get { return _containerManager; }
        }
    }
}
