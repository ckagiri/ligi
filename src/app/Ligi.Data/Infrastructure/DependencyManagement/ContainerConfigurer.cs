﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ligi.Data.Configuration;

namespace Ligi.Data.Infrastructure.DependencyManagement
{
    /// <summary>
    /// Configures the inversion of control container with services used by Ligi.
    /// </summary>
    public class ContainerConfigurer
    {
        public virtual void Configure(IEngine engine, ContainerManager containerManager, LigiConfig configuration)
        {
            //other dependencies
            containerManager.AddComponentInstance<LigiConfig>(configuration, "ligi.configuration");
            containerManager.AddComponentInstance<IEngine>(engine, "ligi.engine");
            containerManager.AddComponentInstance<ContainerConfigurer>(this, "ligi.containerConfigurer");

            //type finder
            containerManager.AddComponent<ITypeFinder, WebAppTypeFinder>("ligi.typeFinder");

            //register dependencies provided by other assemblies
            var typeFinder = containerManager.Resolve<ITypeFinder>();
            containerManager.UpdateContainer(x =>
            {
                var drTypes = typeFinder.FindClassesOfType<IDependencyRegistrar>();
                var drInstances = new List<IDependencyRegistrar>();
                foreach (var drType in drTypes)
                    drInstances.Add((IDependencyRegistrar)Activator.CreateInstance(drType));
                //sort
                drInstances = drInstances.AsQueryable().OrderBy(t => t.Order).ToList();
                foreach (var dependencyRegistrar in drInstances)
                    dependencyRegistrar.Register(x, typeFinder);
            });
        }
    }
}
