﻿using Ligi.Core.Domain;
using System.Data.Entity.ModelConfiguration;

namespace Ligi.Data.Mapping
{
    public class TeamMap : EntityTypeConfiguration<Team>
    {
        public TeamMap()
        {
            ToTable("Team");
            Ignore(t => t.TeamType);
        }
    }
}
