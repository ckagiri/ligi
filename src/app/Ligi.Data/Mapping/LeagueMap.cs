﻿using Ligi.Core.Domain;
using System.Data.Entity.ModelConfiguration;

namespace Ligi.Data.Mapping
{
    public class LeagueMap : EntityTypeConfiguration<League>
    {
        public LeagueMap()
        {
            ToTable("League");
            Property(l => l.Name).IsRequired().HasMaxLength(50);
            Ignore(l => l.Region);
            Ignore(l => l.TeamType);
        }
    }
}
