﻿using Ligi.Core.Domain;
using System.Data.Entity.ModelConfiguration;

namespace Ligi.Data.Mapping
{
    public class PredictionMap : EntityTypeConfiguration<Prediction>
    {
        public PredictionMap()
        {
            ToTable("Prediction");
        }
    }
}
