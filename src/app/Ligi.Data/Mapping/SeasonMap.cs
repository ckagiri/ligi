﻿using Ligi.Core.Domain;
using System.Data.Entity.ModelConfiguration;

namespace Ligi.Data.Mapping
{
    public class SeasonMap : EntityTypeConfiguration<Season>
    {
        public SeasonMap()
        {
            ToTable("Season");
            HasRequired(s => s.League).WithMany(l => l.Seasons);
        }
    }
}
