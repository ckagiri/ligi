﻿using Ligi.Core.Domain;
using System.Data.Entity.ModelConfiguration;

namespace Ligi.Data.Mapping
{
    public class FixtureMap : EntityTypeConfiguration<Fixture>
    {
        public FixtureMap()
        {
            ToTable("Fixture");
            Ignore(f => f.MatchStatus);
            HasRequired(f => f.HomeTeam)
                .WithMany()
                .HasForeignKey(f => f.HomeTeamId)
                .WillCascadeOnDelete(false);
            HasRequired(f => f.AwayTeam)
                .WithMany()
                .HasForeignKey(f => f.AwayTeamId)
                .WillCascadeOnDelete(false);
            HasRequired(l => l.Round)
                .WithMany();
        }
    }
}
