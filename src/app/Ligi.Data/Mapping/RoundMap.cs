﻿using Ligi.Core.Domain;
using System.Data.Entity.ModelConfiguration;

namespace Ligi.Data.Mapping
{
    public class RoundMap : EntityTypeConfiguration<Round>
    {
        public RoundMap()
        {
            ToTable("Round");
        }
    }
}
