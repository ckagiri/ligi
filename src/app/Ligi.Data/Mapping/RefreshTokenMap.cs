﻿using System.Data.Entity.ModelConfiguration;
using Ligi.Data.Security;

namespace Ligi.Data.Mapping
{
    public class RefreshTokenMap : EntityTypeConfiguration<RefreshToken>
    {
        public RefreshTokenMap()
        {
            ToTable("RefreshToken");
            Property(t => t.Subject).IsRequired().HasMaxLength(50);
            Property(t => t.ClientId).IsRequired().HasMaxLength(50);
            Property(t => t.ProtectedTicket).IsRequired();
        }
    }
}
