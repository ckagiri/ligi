﻿using System.Data.Entity.ModelConfiguration;
using Ligi.Core.Domain;

namespace Ligi.Data.Mapping
{
    public class SeasonTeamMap : EntityTypeConfiguration<SeasonTeam>
    {
        public SeasonTeamMap()
        {
            ToTable("Season_Team_Mapping");

            HasRequired(st => st.Team)
                .WithMany()
                .HasForeignKey(st => st.TeamId);
            
            HasRequired(st => st.Season)
                .WithMany(p => p.SeasonTeams)
                .HasForeignKey(st => st.TeamId);
        }
    }
}
