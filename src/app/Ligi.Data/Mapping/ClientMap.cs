﻿using System.Data.Entity.ModelConfiguration;
using Ligi.Data.Security;

namespace Ligi.Data.Mapping
{
    public class ClientMap : EntityTypeConfiguration<Client>
    {
        public ClientMap()
        {
            ToTable("Client");
            Property(c => c.Name).IsRequired().HasMaxLength(100);
            Property(c => c.AllowedOrigin).HasMaxLength(100);
            Ignore(c => c.ApplicationType);
        }
    }
}
