using Ligi.Core.Domain;
using Ligi.Data.Data;

namespace Ligi.Data.Migrations
{
    using System.Data.Entity.Migrations;

    public sealed class Configuration : DbMigrationsConfiguration<LigiDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(LigiDbContext context)
        {
            context.DbSet<League>().AddOrUpdate(
              l => l.Name,
              new League(){ Name = "English Premier League", Code = "EPL", Region = Region.Europe}
            );
        }
    }
}
