﻿using System.Data.Entity;

namespace Ligi.Data.Data
{
    public interface IDbContext
    {
        IDbSet<TEntity> DbSet<TEntity>() where TEntity : class;
        int SaveChanges();
        void Dispose();
    }
}
