﻿using System;
using System.Linq;
using Ligi.Core;

namespace Ligi.Data.Data
{
    public interface IRepository<TEntity> : IRepository<TEntity, int>
        where TEntity : BaseEntity<int>
    {
    }

    public interface IRepository<TEntity, TId>
        where TEntity : BaseEntity<TId>
        where TId : IComparable
    {
        TEntity GetById(TId id);
        void Insert(TEntity entity);
        void Update(TEntity entity);
        void Delete(TEntity entity);
        IQueryable<TEntity> Table { get; }
    }
}
