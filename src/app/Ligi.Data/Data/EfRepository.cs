using System;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using Ligi.Core;

namespace Ligi.Data.Data
{
    public class EfRepository<TEntity> : EfRepository<TEntity, int>
       where TEntity : BaseEntity<int>
    {
        public EfRepository(IDbContext context) : base(context)
        {
        }
    }

    public class EfRepository<TEntity, TId> : IRepository<TEntity, TId> 
        where TEntity : BaseEntity<TId>
        where TId : IComparable
    {
        private readonly IDbContext _dbContext;
        private IDbSet<TEntity> _dbSet;

        public EfRepository(IDbContext context)
        {
            _dbContext = context;
            _dbSet = _dbContext.DbSet<TEntity>();
        }

        public virtual TEntity GetById(TId id)
        {
            TEntity entity = Filter(DbSet, x => x.Id, id).FirstOrDefault();
            return entity;
        }

        public virtual void Insert(TEntity entity)
        {
            try
            {
                if (entity == null)
                    throw new ArgumentNullException("entity");

                DbSet.Add(entity);

                _dbContext.SaveChanges();
            }
            catch (DbEntityValidationException dbEx)
            {
                var msg = string.Empty;

                foreach (var validationErrors in dbEx.EntityValidationErrors)
                    foreach (var validationError in validationErrors.ValidationErrors)
                        msg += string.Format("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage) + Environment.NewLine;

                var fail = new Exception(msg, dbEx);
                //Debug.WriteLine(fail.Message, fail);
                throw fail;
            }
        }

        public virtual void Update(TEntity entity)
        {
            try
            {
                if (entity == null)
                    throw new ArgumentNullException("entity");

                _dbContext.SaveChanges();
            }
            catch (DbEntityValidationException dbEx)
            {
                var msg = string.Empty;

                foreach (var validationErrors in dbEx.EntityValidationErrors)
                    foreach (var validationError in validationErrors.ValidationErrors)
                        msg += Environment.NewLine + string.Format("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);

                var fail = new Exception(msg, dbEx);
                //Debug.WriteLine(fail.Message, fail);
                throw fail;
            }
        }

        public virtual void Delete(TEntity entity)
        {
            try
            {
                if (entity == null)
                    throw new ArgumentNullException("entity");

                DbSet.Remove(entity);

                _dbContext.SaveChanges();
            }
            catch (DbEntityValidationException dbEx)
            {
                var msg = string.Empty;

                foreach (var validationErrors in dbEx.EntityValidationErrors)
                    foreach (var validationError in validationErrors.ValidationErrors)
                        msg += Environment.NewLine + string.Format("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);

                var fail = new Exception(msg, dbEx);
                //Debug.WriteLine(fail.Message, fail);
                throw fail;
            }
        }

        public virtual IQueryable<TEntity> Table
        {
            get
            {
                return DbSet;
            }
        }

        protected virtual IDbSet<TEntity> DbSet
        {
            get
            {
                if (_dbSet == null)
                    _dbSet = _dbContext.DbSet<TEntity>();
                return _dbSet;
            }
        }

        private IQueryable<TEntity> Filter<TProperty>(IQueryable<TEntity> dbSet,
           Expression<Func<TEntity, TProperty>> property, TProperty value)
           where TProperty : IComparable
        {
            var memberExpression = property.Body as MemberExpression;
            if (memberExpression == null || !(memberExpression.Member is PropertyInfo))
            {
                throw new ArgumentException("Property expected", "property");
            }

            Expression left = property.Body;
            Expression right = Expression.Constant(value, typeof (TProperty));
            Expression searchExpression = Expression.Equal(left, right);
            Expression<Func<TEntity, bool>> lambda = Expression.Lambda<Func<TEntity, bool>>(
                searchExpression, new ParameterExpression[] {property.Parameters.Single()});

            return dbSet.Where(lambda);
        }
    }
}