﻿using System.Collections.Generic;

namespace Ligi.Core.Domain
{
    public class Team : BaseEntity
    {
        private ICollection<Fixture> _fixtures;

        public string Name { get; set; }
        public string Code { get; set; }
        public string HomeGround { get; set; }
        public string Tags { get; set; }
        public string ImageSource { get; set; }
        public int TeamTypeId { get; set; }

        public virtual ICollection<Fixture> Fixtures 
        {
            get
            {
                return _fixtures ?? (_fixtures = new List<Fixture>());
            }
            protected set { _fixtures = value; }
        }

        public TeamType TeamType
        {
            get { return (TeamType) TeamTypeId; }
            set { TeamTypeId = (int) value; }
        }
    }
}
