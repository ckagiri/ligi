﻿using System;
using System.Collections.Generic;

namespace Ligi.Core.Domain
{
    public class Season : BaseEntity
    {
        private ICollection<Round> _rounds;
        private ICollection<SeasonTeam> _seasonTeams;

        public string Name { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int LeagueId { get; set; }
        public bool IsPublished { get; set; }

        public virtual League League { get; set; }

        public virtual ICollection<Round> Rounds
        {
            get { return _rounds ?? (_rounds = new List<Round>()); }
            protected set { _rounds = value; }
        } 

        public virtual ICollection<SeasonTeam> SeasonTeams
        {
            get
            {
                return _seasonTeams ?? (_seasonTeams = new List<SeasonTeam>());
            }
            protected set { _seasonTeams = value; }
        }
    }
}
