﻿using System;

namespace Ligi.Core.Domain
{
    public class LeaderBoard
    {
        public int Rank { get; set; }
        public Guid UserId { get; set; }
        public string UserName { get; set; }
        public int LeagueId { get; set; }
        public int SeasonId { get; set; }
        public int Points { get; set; }
        public int GoalDifference { get; set; }
        public int FirstPredictionTimestamp { get; set; }
    }
}
