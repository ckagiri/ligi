﻿using System;

namespace Ligi.Core.Domain
{
    public class Round : BaseEntity
    {
        public int LeagueId { get; set; }
        public int SeasonId { get; set; }
        public string Name { get; set; }
        public DateTime StartDate { get; set; }

        public virtual League League { get; set; }
        public virtual Season Season { get; set; }
    }
}
