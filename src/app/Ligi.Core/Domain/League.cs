﻿using System.Collections.Generic;

namespace Ligi.Core.Domain
{
    public class League : BaseEntity
    {
        private ICollection<Season> _seasons;

        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public int RegionId { get; set; }
        public int TeamTypeId { get; set; }

        public virtual ICollection<Season> Seasons
        {
            get { return _seasons ?? (_seasons = new List<Season>()); }
            protected set { _seasons = value; }
        }

        public Region Region
        {
            get { return (Region) RegionId; }
            set { RegionId = (int) value; }
        }

        public TeamType TeamType
        {
            get { return (TeamType) TeamTypeId; }
            set { TeamTypeId = (int) value; }
        }
    }
}
