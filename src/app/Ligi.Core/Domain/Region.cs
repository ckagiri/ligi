﻿namespace Ligi.Core.Domain
{
    public enum Region
    {
        None,
        World,
        Europe,
        Africa,
        SouthAmerica,
        Asia,
        Australia,
    }
}
