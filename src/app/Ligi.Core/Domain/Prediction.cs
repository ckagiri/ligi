﻿using System;

namespace Ligi.Core.Domain
{
    public class Prediction : BaseEntity<Guid>
    {
        public Guid UserId { get; set; }
        public int LeagueId { get; set; }   
        public int SeasonId { get; set; }
        public int FixtureId { get; set; }
        public string Match { get; set; }
        public int PredictedHomeGoals { get; set; }
        public int PredictedAwayGoals { get; set; }
        public int ActualHomeGoals { get; set; }
        public int ActualAwayGoals { get; set; }
        public int Points { get; set; }
        public int SpreadDifference { get; set; }
        public int BasePoints { get; set; }
        public int BonusPoints { get; set; }
        public bool IsProcessed { get; set; }
        public bool HasJoker { get; set; }
        public DateTime FixtureDate { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime ModifiedOn { get; set; }
        
        public virtual Fixture Fixture { get; set; }
    }
}
