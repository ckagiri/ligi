﻿namespace Ligi.Core.Domain
{
    public class SeasonTeam : BaseEntity
    {
        public int SeasonId { get; set; }
        public int TeamId { get; set; }

        public virtual Season Season { get; set; }
        public virtual Team Team { get; set; }
    }
}
