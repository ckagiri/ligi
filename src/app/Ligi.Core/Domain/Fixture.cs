﻿using System;

namespace Ligi.Core.Domain
{
    public class Fixture : BaseEntity
    {
        public int SeasonId { get; set; }
        public int LeagueId { get; set; }
        public int RoundId { get; set; }
        public int HomeTeamId { get; set; }
        public int AwayTeamId { get; set; }
        public DateTime KickOff { get; set; }
        public string Venue { get; set; }
        public string Match { get; set; }
        public int MatchStatusId { get; set; }
        public int HomeScore { get; set; }
        public int AwayScore { get; set; }
        public bool CanPredict { get; set; }
        public bool IsPublished { get; set; }
        public bool IsConfirmed { get; set; }
        public string HomeTeamImageSource { get; set; }
        public string AwayTeamImageSource { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime ModifiedOn { get; set; }

        public virtual League League { get; set; }
        public virtual Season Season { get; set; }
        public virtual Round Round { get; set; }
        public virtual Team HomeTeam { get; set; }
        public virtual Team AwayTeam { get; set; }

        public MatchStatus MatchStatus
        {
            get { return (MatchStatus) MatchStatusId; }
            set { MatchStatusId = (int) value; }
        }
    }
}
