﻿using Microsoft.AspNet.Identity.EntityFramework;

namespace Ligi.Core.Domain
{
    public class ApplicationUser : IdentityUser
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
