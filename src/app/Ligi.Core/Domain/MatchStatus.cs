﻿namespace Ligi.Core.Domain
{
    public enum MatchStatus
    {
        Scheduled,
        InProgress,
        PendingResult,
        Played,
        Cancelled,
        Abandoned,
        PostPoned
    }
}
