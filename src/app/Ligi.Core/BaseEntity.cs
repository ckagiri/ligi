﻿using System;

namespace Ligi.Core
{
    public class BaseEntity : BaseEntity<int>
    { }

    public abstract class BaseEntity<TId>
        where TId : IComparable
    {
        public TId Id { get; set; }
    }
}
