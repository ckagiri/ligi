﻿using System.Linq;
using Autofac;
using Autofac.Integration.Mvc;
using Autofac.Integration.WebApi;
using Ligi.Core.Domain;
using Ligi.Data.Data;
using Ligi.Data.Infrastructure;
using Ligi.Data.Infrastructure.DependencyManagement;
using Ligi.Data.Security;
using Ligi.Services.Leagues;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Ligi.Web.Api.Infrastructure
{
        public class DependencyRegistrar : IDependencyRegistrar
        {
            public virtual void Register(ContainerBuilder builder, ITypeFinder typeFinder)
            {
                //controllers
                builder.RegisterApiControllers(typeFinder.GetAssemblies().ToArray());
                builder.RegisterControllers(typeFinder.GetAssemblies().ToArray());

                //data layer
                    builder.Register<IDbContext>(c => new LigiDbContext())
                        .InstancePerRequest();
                builder.RegisterGeneric(typeof(EfRepository<>)).As(typeof(IRepository<>)).InstancePerRequest();
                builder.RegisterGeneric(typeof(EfRepository<,>)).As(typeof(IRepository<,>)).InstancePerRequest();
                builder.Register(c => new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new LigiDbContext())
                {
                    /*Avoids UserStore invoking SaveChanges on every actions.*/
                    //AutoSaveChanges = false
                })).As<UserManager<ApplicationUser>>().InstancePerRequest();
                builder.RegisterType<AuthRepository>().As<IAuthRepository>().InstancePerRequest();
                builder.RegisterType<LeagueService>().As<ILeagueService>().InstancePerRequest();
            }

            public int Order
            {
                get { return 1; }
            }
        }
}