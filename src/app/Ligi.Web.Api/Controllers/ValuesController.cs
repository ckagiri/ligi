﻿using System.Collections.Generic;
using System.Web.Http;
using Ligi.Data.Infrastructure;
using Ligi.Data.Security;

namespace Ligi.Web.Api.Controllers
{
    public class ValuesController : ApiController
    {
        private readonly IAuthRepository _authRepository;

        public ValuesController()
        {
            _authRepository = EngineContext.Current.Resolve<IAuthRepository>();
        }

        // GET api/values
        public IEnumerable<string> Get()
        {
            var repo = _authRepository.FindUser("ligi", "ligi");
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }
    }
}
