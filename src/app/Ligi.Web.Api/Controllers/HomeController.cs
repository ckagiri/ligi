﻿using System.Web.Mvc;
using Ligi.Services.Leagues;

namespace Ligi.Web.Api.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILeagueService _leagueService;

        public HomeController(ILeagueService leagueService)
        {
            _leagueService = leagueService;
        }

        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";
            var leagues = _leagueService.GetAllLeagues();
            return View();
        }
    }
}
