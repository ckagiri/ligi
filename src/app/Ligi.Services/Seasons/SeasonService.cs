﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ligi.Core.Domain;
using Ligi.Data.Data;

namespace Ligi.Services.Seasons
{
    public class SeasonService : ISeasonService
    {
        private readonly IRepository<Season> _seasonRepository;
        private readonly IRepository<Team> _teamRepository;
        private readonly IRepository<SeasonTeam> _seasonTeamRepository;

        public SeasonService(IRepository<Season> seasonRepository, 
            IRepository<Team> teamRepository, 
            IRepository<SeasonTeam> seasonTeamRepository)
        {
            _seasonRepository = seasonRepository;
            _teamRepository = teamRepository;
            _seasonTeamRepository = seasonTeamRepository;
        }

        public Season GetSeasonById(int seasonId)
        {
            if (seasonId == 0)
                return null;
            var season = _seasonRepository.GetById(seasonId);
            return season;
        }

        public IList<SeasonTeam> GetAllSeasonTeamsByLeagueId(int leagueId)
        {
            var query =
                from st in _seasonTeamRepository.Table
                join s in _seasonRepository.Table on st.SeasonId equals s.Id
                select st;
            var result = query.ToList();
            return result;
        }

        public IList<SeasonTeam> GetAllSeasonTeamsByTeamId(int teamId)
        {
            var query =
                from st in _seasonTeamRepository.Table
                join t in _teamRepository.Table on st.TeamId equals t.Id
                select st;
            var result = query.ToList();
            return result;
        }

        public void InsertSeason(Season season)
        {
            if (season == null)
                throw new ArgumentNullException("season");

            _seasonRepository.Insert(season);
        }

        public void UpdateSeason(Season season)
        {
            if (season == null)
                throw new ArgumentNullException("season");

            _seasonRepository.Update(season);
        }

        public void DeleteSeason(Season season)
        {
            if (season == null)
                throw new ArgumentNullException("season");

            _seasonRepository.Delete(season);
        }

        public SeasonTeam GetSeasonTeamById(int seasonTeamId)
        {
            if (seasonTeamId == 0)
                return null;
            var seasonTeam = _seasonTeamRepository.GetById(seasonTeamId);
            return seasonTeam;
        }

        public void InsertSeasonTeam(SeasonTeam seasonTeam)
        {
            if (seasonTeam == null)
                throw new ArgumentNullException("seasonTeam");

            _seasonTeamRepository.Insert(seasonTeam);
        }

        public void UpdateSeasonTeam(SeasonTeam seasonTeam)
        {
            if (seasonTeam == null)
                throw new ArgumentNullException("seasonTeam");

            _seasonTeamRepository.Update(seasonTeam);
        }

        public void DeleteSeasonTeam(SeasonTeam seasonTeam)
        {
            if (seasonTeam == null)
                throw new ArgumentNullException("seasonTeam");

            _seasonTeamRepository.Delete(seasonTeam);
        }
    }
}
