﻿using System.Collections.Generic;
using Ligi.Core.Domain;

namespace Ligi.Services.Seasons
{
    public interface ISeasonService
    {
        IList<SeasonTeam> GetAllSeasonTeamsByLeagueId(int leagueId);
        IList<SeasonTeam> GetAllSeasonTeamsByTeamId(int teamId);
        SeasonTeam GetSeasonTeamById(int seasonTeamId);
        Season GetSeasonById(int seasonTeamId);
        void InsertSeason(Season season);
        void UpdateSeason(Season season);
        void DeleteSeason(Season season);
    }
}
