﻿using System.Collections.Generic;
using Ligi.Core.Domain;

namespace Ligi.Services.Teams
{
    public interface ITeamService
    {
        IList<Team> GetAllTeams();
        IList<Team> GetAllTeamsByType(int teamTypeId);
        IList<Team> GetAllTeamsByTag(string tag);
        void InsertTeam(Team team);
        void UpdateTeam(Team team);
        void DeleteTeam(Team team);
    }
}
