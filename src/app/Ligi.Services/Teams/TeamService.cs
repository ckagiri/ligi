﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ligi.Core.Domain;
using Ligi.Data.Data;

namespace Ligi.Services.Teams
{
    public class TeamService : ITeamService
    {
        private readonly IRepository<Team> _teamRepository;

        public TeamService(IRepository<Team> teamRepository)
        {
            _teamRepository = teamRepository;
        }

        public IList<Team> GetAllTeams()
        {
            var query = _teamRepository.Table;
            var result = query.ToList();
            return result;
        }

        public IList<Team> GetAllTeamsByType(int teamTypeId)
        {
            var query = _teamRepository.Table.Where(t => t.TeamTypeId == teamTypeId);
            var result = query.ToList();
            return result;
        }

        public IList<Team> GetAllTeamsByTag(string tag)
        {
            var query = _teamRepository.Table;
            var result = query.ToList();
            return result;
        }

        public virtual void InsertTeam(Team team)
        {
            if (team == null)
                throw new ArgumentNullException("team");

            _teamRepository.Insert(team);
        }

        public void UpdateTeam(Team team)
        {
            if (team == null)
                throw new ArgumentNullException("team");

            _teamRepository.Update(team);
        }

        public void DeleteTeam(Team team)
        {
            if (team == null)
                throw new ArgumentNullException("team");

            _teamRepository.Delete(team);
        }
    }
}
