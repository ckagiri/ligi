﻿using System.Collections.Generic;
using Ligi.Core.Domain;

namespace Ligi.Services.Leagues
{
    public interface ILeagueService
    {
        IList<League> GetAllLeagues();
        void InsertLeague(League league);
        void UpdateLeague(League league);
        void DeleteLeague(League league);
    }
}
