﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ligi.Core.Domain;
using Ligi.Data.Data;

namespace Ligi.Services.Leagues
{
    public class LeagueService : ILeagueService
    {
        private readonly IRepository<League> _leagueRepository;

        public LeagueService(IRepository<League> leagueRepository)
        {
            _leagueRepository = leagueRepository;
        }

        public IList<League> GetAllLeagues()
        {
            var query = _leagueRepository.Table;
            var result = query.ToList();
            return result;
        }

        public void InsertLeague(League league)
        {
            if (league == null)
                throw new ArgumentNullException("league");

            _leagueRepository.Insert(league);
        }

        public void UpdateLeague(League league)
        {
            if (league == null)
                throw new ArgumentNullException("league");

            _leagueRepository.Update(league);
        }

        public void DeleteLeague(League league)
        {
            if (league == null)
                throw new ArgumentNullException("league");

            _leagueRepository.Delete(league);
        }
    }
}
