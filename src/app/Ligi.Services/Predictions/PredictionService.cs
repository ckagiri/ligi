﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ligi.Core.Domain;
using Ligi.Data.Data;

namespace Ligi.Services.Predictions
{
    public class PredictionService : IPredictionService
    {
        private readonly IRepository<Prediction, Guid> _predictionRepository;

        public PredictionService(IRepository<Prediction, Guid> predictionRepository)
        {
            _predictionRepository = predictionRepository;
        }

        public IList<Prediction> GetAllPredictionsByUserId(Guid userId)
        {
            var query = _predictionRepository.Table;
            var result = query.Where(p => p.UserId == userId).ToList();
            return result;
        }

        public void InsertPrediction(Prediction prediction)
        {
            if (prediction == null)
                throw new ArgumentNullException("prediction");

            _predictionRepository.Insert(prediction);
        }

        public void UpdatePrediction(Prediction prediction)
        {
            if (prediction == null)
                throw new ArgumentNullException("prediction");

            _predictionRepository.Update(prediction);
        }
    }
}
