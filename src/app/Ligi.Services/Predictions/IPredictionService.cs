﻿using System;
using System.Collections.Generic;
using Ligi.Core.Domain;

namespace Ligi.Services.Predictions
{
    public interface IPredictionService
    {
        IList<Prediction> GetAllPredictionsByUserId(Guid userId);
        void InsertPrediction(Prediction prediction);
        void UpdatePrediction(Prediction prediction);
    }
}
