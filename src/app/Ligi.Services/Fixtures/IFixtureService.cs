﻿using System.Collections.Generic;
using Ligi.Core.Domain;

namespace Ligi.Services.Fixtures
{
    public interface IFixtureService
    {
        IList<Fixture> GetAllFixturesBySeasonId(int seasonId);
        void InsertFixture(Fixture fixture);
        void UpdateFixture(Fixture fixture);
        void DeleteFixture(Fixture fixture);
    }
}
