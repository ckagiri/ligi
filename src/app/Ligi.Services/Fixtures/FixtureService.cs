﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ligi.Core.Domain;
using Ligi.Data.Data;

namespace Ligi.Services.Fixtures
{
    public class FixtureService : IFixtureService
    {
        private readonly IRepository<Fixture> _fixtureRepository;

        public FixtureService(IRepository<Fixture> fixtureRepository)
        {
            _fixtureRepository = fixtureRepository;
        }

        public IList<Fixture> GetAllFixturesBySeasonId(int seasonId)
        {
            var query = _fixtureRepository.Table;
            var result = query.Where(f => f.SeasonId == seasonId).ToList();
            return result;
        }

        public void InsertFixture(Fixture fixture)
        {
            if (fixture == null)
                throw new ArgumentNullException("fixture");

            _fixtureRepository.Insert(fixture);
        }

        public void UpdateFixture(Fixture fixture)
        {
            if (fixture == null)
                throw new ArgumentNullException("fixture");

            _fixtureRepository.Update(fixture);
        }

        public void DeleteFixture(Fixture fixture)
        {
            if (fixture == null)
                throw new ArgumentNullException("fixture");

            _fixtureRepository.Delete(fixture);
        }
    }
}
