﻿using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;

namespace Ligi.Web.Security.Models
{
    public class AppContext : IdentityDbContext<User>
    {
        public AppContext()
            : base("DefaultConnection")
        {
        }

        //Db Sets
        //public virtual DbSet<Partner> Partners { get; set; }

        static AppContext()
        {
            // Set the database intializer which is run once during application start
            // This seeds the database with admin user credentials and admin role
            Database.SetInitializer<AppContext>(new ApplicationDbInitializer());
        }

        public static AppContext Create()
        {
            return new AppContext();
        }
    }
}