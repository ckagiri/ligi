﻿using Ligi.Core.Domain;
using Ligi.Tests;
using NUnit.Framework;

namespace Ligi.Data.Tests.PersistenceTests
{
    [TestFixture]
    public class LeaguePersistenceTests : PersistenceTest
    {
        [Test]
        public void Can_save_and_load_league()
        {
            var league = new League
            {
                Name = "Ligi",
            };

            var fromDb = SaveAndLoadEntity(league);
            fromDb.ShouldNotBeNull();
            fromDb.Name.ShouldEqual("Ligi");
        }
    }
}