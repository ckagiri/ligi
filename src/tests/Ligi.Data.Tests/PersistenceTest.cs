﻿using System;
using Ligi.Core;
using Ligi.Data.Data;
using NUnit.Framework;

namespace Ligi.Data.Tests
{
    [TestFixture]
    public abstract class PersistenceTest
    {
        protected LigiDbContext context;

        [SetUp]
        public void SetUp()
        {
            //TODO fix compilation warning (below)
            #pragma warning disable 0618
            context = new LigiDbContext();
            context.Database.Delete();
            context.Database.Create();
        }

        protected TEntity SaveAndLoadEntity<TEntity>(TEntity entity, bool disposeContext = true) where TEntity: BaseEntity
        {
            return SaveAndLoadEntity<TEntity, int>(entity, disposeContext);
        }
      
        protected TEntity SaveAndLoadEntity<TEntity, TId>(TEntity entity, bool disposeContext = true) 
            where TEntity :  BaseEntity<TId> where TId: IComparable
        {
            context.DbSet<TEntity>().Add(entity);
            context.SaveChanges();
            if (disposeContext)
            {
                context.Dispose();
                context = new LigiDbContext();
            }

            var fromDb = context.DbSet<TEntity>().Find(entity.Id);
            return fromDb;
        }
    }
}
